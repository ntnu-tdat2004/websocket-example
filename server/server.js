'use strict';

let express = require('express');
let WebSocket = require('ws');

let httpApplication = express();
httpApplication.use(express.static(__dirname + '/../client'));
let httpServer = httpApplication.listen(3000, () =>
  console.log('HTTP server listening at port', httpServer.address().port)
);

let wsServer = new WebSocket.Server({ port: 3001 });
wsServer.on('connection', connection => {
  console.log('Client connected');

  connection.on('message', message => {
    console.log('message received from client:', message);

    // Send message to all clients:
    for (let client of wsServer.clients) {
      if (client.readyState == WebSocket.OPEN) client.send(message);
    }
  });

  connection.on('close', () => {
    console.log('Client disconnected');
  });

  connection.on('error', error => {
    console.error('Error:', error.message);
  });
});
